Controls
--------
Animation and curve editing controls are displayed in the window directly. 
Other controls are:
R -> Reset the scene.
ESC -> Quit the application. 
Z -> Toggle object visibility. 
X -> Toggel Info visibility. 
CTRL + O -> Open new curve file. 
CTRL + S -> Save curve to file. 

Note Curves. 
	Each curve point is given as a keyframe. That is, a [time-value] pair. This is true for tangents and control points for hermite and bezier curve, respectively. In this case, the time value is ignored, only the time for the end points that are interpolated matters. 
	
	The editor works with 2D curves and so it won't save the Z value for each point, tangent and control points. Although it can load curves containing 3D points. If you wish to use the editor to edit curves and load them in your program, you will have to manually add the Z coordinate for each value. 
	
